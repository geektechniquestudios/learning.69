package com.geektechnique.practice2.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
//@AllArgsConstructor
@NoArgsConstructor
public class Fish {

    private UUID id;
    private String breed;

    public Fish(@JsonProperty("id") UUID id, @JsonProperty("breed") String breed){
        this.id = id;
        this.breed = breed;
    }



}
