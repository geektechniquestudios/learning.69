package com.geektechnique.practice2.dao;

import com.geektechnique.practice2.model.Fish;
import org.apache.catalina.User;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class FishDaoImpl implements FishDao {

    private RedisTemplate<String, Fish> redisTemplate;

    private HashOperations hashOperations;

    public FishDaoImpl(RedisTemplate<String, Fish> redisTemplate) {
        this.redisTemplate = redisTemplate;
        hashOperations = redisTemplate.opsForHash();
    }

    @Override
    public int insertFish(UUID id, Fish fish) {

        return 0;
    }

    @Override
    public List<Fish> selectAllFish() {
        return null;
    }

    @Override
    public Optional<Fish> selectFishByID(UUID id) {
        return Optional.empty();
    }

    @Override
    public int deleteFishById(UUID id) {
        return 0;
    }

    @Override
    public int updateFishById(UUID id, Fish fish) {
        return 0;
    }
}
