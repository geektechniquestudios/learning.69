package com.geektechnique.practice2.dao;


import com.geektechnique.practice2.model.Fish;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface FishDao {
    int insertFish(UUID id, Fish fish);

    default int insertFish(Fish fish){
        UUID id = UUID.randomUUID();
        return insertFish(id, fish);
    }

    List<Fish> selectAllFish();

    Optional<Fish> selectFishByID(UUID id);

    int deleteFishById(UUID id);

    int updateFishById(UUID id, Fish fish);

}
